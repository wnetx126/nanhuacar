<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>南华汽车网</title>
<link href="style.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="js/jquery.js"></script>
<script language="javascript" src="js/nhcar.js"></script>
<script language="javascript" src="js/banner.js"></script>
</head>

<body>
<div id="header">
	<div id="header-content">
        <a href="#" id="logo"><img src="images/logo.gif" alt="南华汽车网" /></a>
        <div id="header-right">
        <ul id="subnav">
             <?php
                //var_dump($_COOKIE);
                //isset(变量);该函数可以判断变量是否设置有值 返回true或flase
                if(isset($_COOKIE['nhuid'])){
                    //echo"<li>登录成功</li>";
            ?>
                <li>您好<?php echo $_COOKIE['nhuname'] ;?>,欢迎来到南华汽车！[<a href="login.php">注销</ a>]</li>
            <?php
                } else {
                    //echo"<li>登录失败</li>";
            ?>
                <li>您好，欢迎来到南华汽车![<a href="login.php">登录</ a>][<a href="register.php">免费注册</ a>]</li>
            <?php
                }
            ?>
            <li class="mobile"><a href="#">手机版</a></li>
            <li class="car"><a href="#">购物车</a></li>
            <li class="message"><a href="#">在线留言</a></li>
        </ul>
        <form id="search">
            <p><label>输入你要搜索的车型名称</label><input type="text"  value="" class="skey"/><input type="submit" value="搜索" class="searchbt" /></p>
            <dl>
                <dt>热门：</dt>
                <dd><a href="#">速腾</a></dd>
                <dd><a href="#">奥迪A3</a></dd>
                <dd><a href="#">宝马X6</a></dd>
                <dd><a href="#">速腾</a></dd>
                <dd><a href="#">奥迪A3</a></dd>
                <dd><a href="#">宝马X6</a></dd>
            </dl>
        </form>
        </div>
      <div id="nav">
        	<div class="catelist">
            	<h2>汽车分类</h2>
            	<ul>
            	  <li><a href="#">大众</a></li>
            	  <li><a href="#">丰田</a></li>
            	  <li><a href="#">吉利</a></li>
            	  <li><a href="#">海马</a></li>
            	  <li><a href="#">奥迪</a></li>
            	  <li><a href="#">劳斯莱斯</a></li>
            	  <li><a href="#">雷克萨斯</a></li>
            	  <li><a href="#">本田</a></li>
            	  <li><a href="#">英菲尼迪</a></li>
            	  <li><a href="#">凯迪拉克</a></li>
            	  <li><a href="#">奇瑞</a></li>
              </ul>
       	  </div>
          <ul id="navlist">
              <li><a href="#">首页</a></li>
                <li><a href="productlist.php">新车上市</a></li>
            <li><a href="#">热销汽车</a></li>
            <li><a href="#">豪车推荐</a></li>
            <li><a href="#">汽车资讯</a></li>
          </ul>
      </div>
    </div>
</div>
<div id="banner">
    <div id="banner-content">
        <ul class="bannerlist">
            <li ><a href="#"><img src="images/banner1.jpg" /></a></li>
            <li><a href="#"><img src="images/banner2.jpg" /></a></li>
            <li><a href="#"><img src="images/banner3.jpg" /></a></li> 
        </ul>
        <a href="#" id="leftarrow">left</a>
        <a href="#" id="rightarrow">right</a>
    </div>
 </div>
<div id="main">
	<div id="content">
        <div id="subcategory">
            <h2>品牌：</h2>
            <ul>
                  <li><a href="#">大众</a></li>
                  <li><a href="#">丰田</a></li>
                  <li><a href="#">吉利</a></li>
                  <li><a href="#">海马</a></li>
                  <li><a href="#">奥迪</a></li>
                  <li><a href="#">劳斯莱斯</a></li>
                  <li><a href="#">雷克萨斯</a></li>
                  <li><a href="#">本田</a></li>
                  <li><a href="#">英菲尼迪</a></li>
                  <li><a href="#">凯迪拉克</a></li>
                  <li><a href="#">奇瑞</a></li>
                  <li><a href="#">大众</a></li>
                  <li><a href="#">丰田</a></li>
                  <li><a href="#">吉利</a></li>
                  <li><a href="#">海马</a></li>
                  <li><a href="#">奥迪</a></li>
                  <li><a href="#">劳斯莱斯</a></li>
                  <li><a href="#">雷克萨斯</a></li>
                  <li><a href="#">本田</a></li>
                  <li><a href="#">英菲尼迪</a></li>
                  <li><a href="#">凯迪拉克</a></li>
                  <li><a href="#">奇瑞</a></li>
                  <li><a href="#">大众</a></li>
                  <li><a href="#">丰田</a></li>
                  <li><a href="#">吉利</a></li>
                  <li><a href="#">海马</a></li>
                  <li><a href="#">奥迪</a></li>
                  <li><a href="#">劳斯莱斯</a></li>
                  <li><a href="#">雷克萨斯</a></li>
                  <li><a href="#">本田</a></li>
                  <li><a href="#">英菲尼迪</a></li>
                  <li><a href="#">凯迪拉克</a></li>
                  <li><a href="#">奇瑞</a></li>
            </ul>
            <div class="clear"></div>
            <a href="#" class="more">more</a>
        </div>
        <div class="homeplist" id="hotplist">
        <h2><a href="#">more</a>热卖车型</h2>
        <ul>
        <?php
        //1.连接数据库
        $link=mysqli_connect("localhost","root","");//创建数据库的连接
        mysqli_select_db($link, "nhcar");//选择操作的数据库
        mysqli_set_charset($link, "utf8");//设置数据库连接的字符编码
        //2.操作数据表，写数据模板
        $sql="SELECT * FROM product WHERE pishot='1';";
        $result=mysqli_query($link,$sql);//mysqli_query如果select返回一个查询结果资源；如果是insert update delete返回的true或false
        //3.处理结果数据
       // $row=mysqli_fetch_assoc($result);//返回一行数据关联数组
        while($row=mysqli_fetch_assoc($result)){
           $pname=$row['pname'];
           $pprice=$row['pprice'];
           $ppic=$row['ppic'];
           $pid=$row['pid'];
        //echo " $pname  $pprice  $ppic<br>";
        ?>
            <li><a href="product.php?pid=<?php echo $pid;?>"><img src="images/<?php echo $ppic;?>"/></a>
                <h3><a href="product.php?pid=<?php echo $pid;?>"><?php echo $pname;?></a></h3>
                <p><a href="product.php?pid=<?php echo $pid;?>">立即抢购</a>RMB:<?php echo $pprice;?></p>
            </li>
        <?php
        }
        ?>
        </ul>
        </div>
        <div class="homeplist" id="newplist">
        <h2><a href="#">more</a>新车上市</h2>
         <ul>
        <?php
        //1.连接数据库
        $link=mysqli_connect("localhost","root","");//创建数据库的连接
        mysqli_select_db($link, "nhcar");//选择操作的数据库
        mysqli_set_charset($link, "utf8");//设置数据库连接的字符编码
        //2.操作数据表，写数据模板
        $sql="SELECT * FROM product WHERE pisnew='1';";
        $result=mysqli_query($link,$sql);//mysqli_query如果select返回一个查询结果资源；如果是insert update delete返回的true或false
        //3.处理结果数据
       // $row=mysqli_fetch_assoc($result);//返回一行数据关联数组
        while($row=mysqli_fetch_assoc($result)){
           $pname=$row['pname'];
           $pprice=$row['pprice'];
           $ppic=$row['ppic'];
           $pid=$row['pid'];
        //echo " $pname  $pprice  $ppic<br>";
        ?>
             <li><a href="product.php?pid=<?php echo $pid;?>"><img src="images/<?php echo $ppic;?>"/></a>
                 <h3><a href="product.php?pid=<?php echo $pid;?>"><?php echo $pname;?></a></h3>
                 <p><a href="product.php?pid=<?php echo $pid;?>">立即抢购</a>RMB:<?php echo $pprice;?></p>
          </li>
        <?php
        }
        ?>
        </ul>
        </div>
        <div class="homeplist" id="cheapplist">
        <h2><a href="#">more</a>促销车型</h2>
        <ul>
        <?php
        //1.连接数据库
        $link=mysqli_connect("localhost","root","");//创建数据库的连接
        mysqli_select_db($link, "nhcar");//选择操作的数据库
        mysqli_set_charset($link, "utf8");//设置数据库连接的字符编码
        //2.操作数据表，写数据模板
        $sql="SELECT * FROM product WHERE pischeap='1';";
        $result=mysqli_query($link,$sql);//mysqli_query如果select返回一个查询结果资源；如果是insert update delete返回的true或false
        //3.处理结果数据
       // $row=mysqli_fetch_assoc($result);//返回一行数据关联数组
        while($row=mysqli_fetch_assoc($result)){
           $pname=$row['pname'];
           $pprice=$row['pprice'];
           $ppic=$row['ppic'];
           $pid=$row['pid'];
        //echo " $pname  $pprice  $ppic<br>";
        ?>
            <li><a href="product.php?pid=<?php echo $pid;?>"><img src="images/<?php echo $ppic;?>" /></a>
                <h3><a href="product.php?pid=<?php echo $pid;?>"><?php echo $pname;?></a></h3>
                <p><a href="product.php?pid=<?php echo $pid;?>">立即抢购</a>RMB:<?php echo $pprice;?></p>
         </li>
        <?php
        }
        ?>
        </ul>
        </div>
    </div>
</div>
<div id="footer">
	<ul id="footer-nav">
    	<li><a href="#">免责条款</a></li>
        <li><a href="#">隐私保护</a></li>
        <li><a href="#">咨询热点</a></li>
        <li><a href="#">联系我们</a></li>
        <li><a href="#">公司简介</a></li>
        <li class="last"><a href="#">配送方式</a></li>
    </ul>
    <ul id="footer-intro">
    	<li class="item1">正品保障</li>
        <li class="item2">30天退换货</li>
        <li class="item3">货到付款</li>
        <li class="item4">实物拍摄</li>
        <li class="item5">实时发货</li>
        <li class="item6">会员积分</li>
    </ul>
    <div id="footer-content">
        <p>&copy;2002-2015 南华汽车网（nhcar.com）版权所有，并保留所有权利。ICP备案证书号：粤ICP888888号</p>
        <p>常年法律顾问：南华律师事务所。技术支持：广州锐发计算机科技有限公司</p>
        <p>广州市南华工商学院信息管理专业</p>
    </div>
</div>
<div id="cslist">
	<ul>
    	<li class="qq"><a href="#"><i>qq</i></a></li>
        <li class="tel"><a href="#"><i>tel</i></a></li>
        <li class="weixin"><a href="#"><i>weixin</i></a></li>
        <li class="top"><a href="#"><i>top</i></a></li>
    </ul>
</div>
</body>
</html>
